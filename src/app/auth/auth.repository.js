const User = require('./auth.model');
const { AppError, AppErrorTypes } = require('../error/error');

const findByCredentials = (name, password) => User.findOne({name: name, pwd : password})
  .then(user => {
    if(user){
      return user;
    } else {
      const error = new AppError(AppErrorTypes.RESOURCE_NOT_FOUND,
        `No User with name=${name} & password=${password} has been found`);
      return Promise.reject(error);
    }
  });

const findById = id =>  Users.findById(id)
  .then(user => {
    if (User) {
      return Promise.resolve(User);
    } else {
      const error = new AppError(AppErrorTypes.RESOURCE_NOT_FOUND, `No User with id=${id} has been found`);
      return Promise.reject(error);
    }
  });

module.exports = {
  findByCredentials,
  findById
};
