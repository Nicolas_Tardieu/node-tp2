const toDto = entity => ({
  id: entity.id,
  name: entity.name,
  pwd: entity.pwd
});

const toEntity = dto => ({
  _id: dto.id,
  name: dto.name,
  pwd: dto.pwd
});

module.exports = {
  toDto,
  toEntity
};
