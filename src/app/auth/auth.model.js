const mongoose = require('mongoose');

const schema = mongoose.Schema({
  name: String,
  pwd: String
});

const model = mongoose.model('User', schema);

module.exports = model;
