const repository = require('./library.repository');
const mapper = require('./library.mapper');
const {AppError, AppErrorTypes} = require('../error/error');

const repositoryAuthor = require('../author/author.repository');

const findAll = (req, res, next) => {
  repository.findAll()
    .then(entities => {
      const librarys = entities.map(mapper.toDto);
      res.json(librarys);
    })
    .catch(next)
};

const find = (req, res, next) => {
  const libraryId = req.params.id;
  repository.find(libraryId)
    .then(entity => {
      const library = mapper.toDto(entity);
      res.json(library);
    })
    .catch(next);
};

const create = (req, res, next) => {
  const library = req.body; 
  const authorName = req.body.authors;

  authorId = repositoryAuthor.findAuthor(authorName);

  if (!library) {
    next(new AppError(AppErrorTypes.DTO_INVALID_FORMAT));
  }
  const entity = mapper.toEntity(library);
  repository.create(entity)
    .then(createdEntity => {
      const createdLibrary = mapper.toDto(createdEntity);
      res.status(201).json(createdLibrary);
    })
    .catch(next);
};

const update = (req, res, next) => {
  const library = req.body;
  const libraryId = req.params.id;
  if (!library) {
    return next(new AppError(AppErrorTypes.DTO_INVALID_FORMAT));
  }
  library.id = libraryId;
  const entity = mapper.toEntity(library);
  repository.update(entity)
    .then(() => res.sendStatus(204))
    .catch(next);
};

const remove = (req, res, next) => {
  const libraryId = req.params.id;
  repository.remove(libraryId)
    .then(() => res.sendStatus(204))
    .catch(next);
};

module.exports = {
  findAll,
  find,
 // findTilte,
  create,
  update,
  remove
};