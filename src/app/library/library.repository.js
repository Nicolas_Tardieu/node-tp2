const Library = require('./library.model');
const { AppError, AppErrorTypes } = require('../error/error');

const findAll = () => Library.find();

const find = id => Library.findById(id)
  .then(entity => {
    if (entity) {
      return entity;
    } else {
      const error = new AppError(AppErrorTypes.RESOURCE_NOT_FOUND, `No library with id=${id} has been found`);
      return Promise.reject(error);
    }
  });

/*const findTitle = id => Library.findById(id)
  .then(entity => {
    if (entity) {
      return entity;
    } else {
      const error = new AppError(AppErrorTypes.RESOURCE_NOT_FOUND, `No library with id=${id} has been found`);
      return Promise.reject(error);
    }
  });*/

const create = entity => {
  const library = new Library(entity);
  return library.save();
};

const update = entity => {
  const library = new Library(entity);
  library.isNew = false;
  return library.save()
    .catch(error => {
      if (error.name === 'VersionError') {
        return Promise.reject(new AppError(AppErrorTypes.RESOURCE_NOT_FOUND, `No library with id=${entity.id} has been found`));
      }
      return Promise.reject()
    });
};

const remove = id => Library.deleteOne({ _id: id })
  .then(response => {
    if (!response.n) {
      return Promise.reject(new AppError(AppErrorTypes.RESOURCE_NOT_FOUND, `No library with id=${id} has been found`));
    }
  });

module.exports = {
  findAll,
  find,
// findTitle,
  create,
  update,
  remove
};