const moment = require('moment');

const toDto = entity => ({
  id: entity.id,
  type: entity.type,
  coordinates: [entity.coordinates]
});

const toEntity = dto => ({
  _id: dto.id,
  type: dto.type,
  coordinates: [dto.coordinates]
});

module.exports = {
  toDto,
  toEntity
};
