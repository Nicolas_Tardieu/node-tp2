const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = mongoose.Schema({
  type: String,
  coordinates: Array
});

const Library = mongoose.model('Library', schema);

module.exports = Library;
