const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = mongoose.Schema({
  title: String,
  description: String,
  publicationDate: Number,
  authors: [
    { type: Schema.Types.ObjectId, ref: 'Author' }
  ]
});

const Book = mongoose.model('Book', schema);

module.exports = Book;
