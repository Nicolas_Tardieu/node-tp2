const Book = require('./book.model');
const { AppError, AppErrorTypes } = require('../error/error');

const findAll = () => Book.find();

const find = id => Book.findById(id)
  .then(entity => {
    if (entity) {
      return entity;
    } else {
      const error = new AppError(AppErrorTypes.RESOURCE_NOT_FOUND, `No book with id=${id} has been found`);
      return Promise.reject(error);
    }
  });

/*const findTitle = id => Book.findById(id)
  .then(entity => {
    if (entity) {
      return entity;
    } else {
      const error = new AppError(AppErrorTypes.RESOURCE_NOT_FOUND, `No book with id=${id} has been found`);
      return Promise.reject(error);
    }
  });*/

const create = entity => {
  const book = new Book(entity);
  return book.save();
};

const update = entity => {
  const book = new Book(entity);
  book.isNew = false;
  return book.save()
    .catch(error => {
      if (error.name === 'VersionError') {
        return Promise.reject(new AppError(AppErrorTypes.RESOURCE_NOT_FOUND, `No book with id=${entity.id} has been found`));
      }
      return Promise.reject()
    });
};

const remove = id => Book.deleteOne({ _id: id })
  .then(response => {
    if (!response.n) {
      return Promise.reject(new AppError(AppErrorTypes.RESOURCE_NOT_FOUND, `No book with id=${id} has been found`));
    }
  });

module.exports = {
  findAll,
  find,
// findTitle,
  create,
  update,
  remove
};