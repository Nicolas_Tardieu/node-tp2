const express = require('express');
const authRouter = require('./auth/auth.router');
const authorRouter = require('./author/author.router');
const bookRouter = require('./book/book.router');
const libraryRouter = require('./library/library.router');

const router = express.Router();

router
  .use('/auth', authRouter)
  .use('/author', authorRouter)
  .use('/books', bookRouter)
  .use('/library', libraryRouter);

module.exports = router;
