const Author = require('./author.model');
const { AppError, AppErrorTypes } = require('../error/error');

const findAll = () => Author.find();

const find = id => Author.findById(id)
  .then(entity => {
    if (entity) {
      return entity;
    } else {
      const error = new AppError(AppErrorTypes.RESOURCE_NOT_FOUND, `No author with id=${id} has been found`);
      return Promise.reject(error);
    }
  });

const findAuthor = name => Author.findOne({name: name}, function(err,obj) { 
  if (err) return handleError(err);
  if (obj) {
    return obj._id; 
  }
});


const create = entity => {
  const author = new Author(entity);
  return author.save();
};

const update = entity => {
  const author = new Author(entity);
  author.isNew = false;
  return author.save()
    .catch(error => {
      if (error.name === 'VersionError') {
        return Promise.reject(new AppError(AppErrorTypes.RESOURCE_NOT_FOUND, `No author with id=${entity.id} has been found`));
      }
      return Promise.reject()
    });
};

const remove = id => Author.deleteOne({ _id: id })
  .then(response => {
    if (!response.n) {
      return Promise.reject(new AppError(AppErrorTypes.RESOURCE_NOT_FOUND, `No author with id=${id} has been found`));
    }
  });

module.exports = {
  findAll,
  find,
  findAuthor,
  create,
  update,
  remove
};