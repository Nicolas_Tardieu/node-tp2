
const toDto = entity => ({
  id: entity.id,
  name: entity.name
});

const toEntity = dto => ({
  _id: dto.id,
  name: dto.name
});

module.exports = {
  toDto,
  toEntity
};
