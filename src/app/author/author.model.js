const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = mongoose.Schema({
  //_id: Schema.Types.ObjectId,
  name: String
});

const Author = mongoose.model('Author', schema);

module.exports = Author;
