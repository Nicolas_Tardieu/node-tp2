const express = require('express');
const session = require('express-session');
const passport = require('passport');

require('./auth/auth.config');
require('./common/mongo.config');
const errorMiddleware = require('./error/error.middleware');
const appRouter = require('./app.router');

const app = express();

app.use(express.json());
app.use(session({secret: 'topsecret', resave: false, saveUninitialized: false}));
app.use(passport.initialize());
app.use(passport.session());

app.use(appRouter);
app.use(errorMiddleware);

module.exports = app;
