# NodeJS - INGESUP - B3 - TP n°2

## Prerequisites
- [Node][1] 10
- [NPM][2] 6

## Installation

```
npm install
```

## Usage

### Run
```
npm run start
```

### Debug
```
npm run start:debug
```

### Run dist
```
npm run start:dist
```

### Lint
```
npm run lint
```

### Build
```
npm run build
```

### Endpoints

#### Log IN

curl -X POST http://localhost:3000/auth/login/

json : 

{
    "username": "causette",
    "password": "legenre"
}

#### Books

##### Find all
```
curl -X GET http://localhost:3000/books
```

##### Find
```
curl -X GET http://localhost:3000/books/1
```

##### Create
```
curl -X POST http://localhost:3000/books \
  -H 'Content-Type: application/json' \
  -d '{
    "title": "Harry Potter et le Prince de sang-mêlé",
    "description": "Tome 6",
    "publicationDate": "2005-01-01T01:00:00+01:00",
    "authors": [
        "J. K. Rowling"
    ]
}'
```

##### Update
```
curl -X PUT http://localhost:3000/books/7 \
  -H 'Content-Type: application/json' \
  -d '{
	"id": 7,
    "title": "Harry Potter et les Reliques de la Mort",
    "description": "Tome 7",
    "publicationDate": "2007-01-01T01:00:00+01:00",
    "authors": [
        "J. K. Rowling"
    ]
}'
```

##### Delete
```
curl -X DELETE http://localhost:3000/books/8
```


#### Authors

##### Find all
```
curl -X GET http://localhost:3000/author
```

##### Find
```
curl -X GET http://localhost:3000/author/1
```



#### Library

##### Find all
```
curl -X GET http://localhost:3000/library
```

##### Find
```
curl -X GET http://localhost:3000/library/1
```



## Major dependencies
- [Express][3]
- [Nodemon][4]
- [ESLint][5]
- [Webpack][6]
- [Moment][7]

## Author
Nicolas Tardieu

[1]: https://nodejs.org/en/download/
[2]: https://www.npmjs.com/get-npm
[3]: http://expressjs.com/en/guide/routing.html
[4]: https://github.com/remy/nodemon
[5]: https://eslint.org/docs/user-guide/command-line-interface
[6]: https://webpack.js.org/concepts
[7]: http://momentjs.com
